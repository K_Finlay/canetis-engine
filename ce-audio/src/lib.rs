/*===============================================================================================*/
// Copyright 2018 Fractured Cell, All Rights Reserved.
//
// THIS FILE IS A PART OF Canetis Engine.
// UNAUTHORISED COPYING OF THIS FILE, VIA ANY MEDIUM IS STRICTLY PROHIBITED.
/*===============================================================================================*/

//#![deny (missing_docs)]
#![feature (nll)]

#[macro_use] extern crate log;

extern crate ce_math as math;
extern crate sdl2_sys as sdl2;
extern crate steam_audio_sys as steam_audio;

mod clip;
mod emitter;
mod system;

pub use self::{

    clip::AudioClip,
    emitter::AudioEmitter,
    system::AudioSystem
};

// Constants
const SAMPLE_RATE: usize = 44100;
const BUFFER_SIZE: usize = 8192;
const FRAME_SIZE:  usize = 1024;
