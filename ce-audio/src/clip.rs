/*===============================================================================================*/
// Copyright 2018 Fractured Cell, All Rights Reserved.
//
// THIS FILE IS A PART OF Canetis Engine.
// UNAUTHORISED COPYING OF THIS FILE, VIA ANY MEDIUM IS STRICTLY PROHIBITED.
/*===============================================================================================*/

use std::fs;

pub struct AudioClip {

    pub (in super) stream: fs::File
}
