/*===============================================================================================*/
// Copyright 2018 Fractured Cell, All Rights Reserved.
//
// THIS FILE IS A PART OF Canetis Engine.
// UNAUTHORISED COPYING OF THIS FILE, VIA ANY MEDIUM IS STRICTLY PROHIBITED.
/*===============================================================================================*/

use math::Point3;
use steam_audio::*;

use std::cell::{Cell, RefCell};

pub struct AudioEmitter {

    pub audio_clip: String,

    pub is_3d:      Cell<bool>,
    pub is_looping: Cell<bool>,
    pub is_playing: Cell<bool>,

    pub position: RefCell<Point3>,

    pub (in super) hrtf_effect:  Cell<IPLhandle>,
    pub (in super) frame_offset: Cell<u64>
}

impl AudioEmitter {

    pub fn play (&self) {
        self.is_playing.set (true);
    }

    pub fn pause (&self) {
        self.is_playing.set (false);
    }

    pub fn stop (&self) {

        self.is_playing.set (false);
        self.frame_offset.set (0);
    }
}

impl Drop for AudioEmitter {

    fn drop (&mut self) {
        unsafe { iplDestroyBinauralEffect (self.hrtf_effect.get_mut ()) };
    }
}
