/*===============================================================================================*/
// Copyright 2018 Fractured Cell, All Rights Reserved.
//
// THIS FILE IS A PART OF Canetis Engine.
// UNAUTHORISED COPYING OF THIS FILE, VIA ANY MEDIUM IS STRICTLY PROHIBITED.
/*===============================================================================================*/

use {
    BUFFER_SIZE, FRAME_SIZE, SAMPLE_RATE,
    AudioClip, AudioEmitter
};

use math::Point3;
use sdl2::*;
use steam_audio::*;

use std::{
    cell::{
        Cell,
        RefCell
    },
    cmp,
    collections::HashMap,
    fs::File,
    io::{
        Read,
        Seek,
        SeekFrom
    },
    os::raw,
    path::Path,
    ptr,
    rc::Rc
};

pub struct AudioSystem {

    audio_device: Cell<u32>,
    callback_len: Cell<usize>,

    clip_list:    HashMap<String, AudioClip>,
    emitter_list: Vec<Rc<AudioEmitter>>,
    listener_pos: Point3,

    frame_buffer: [u8; BUFFER_SIZE],

    sa_context:       Cell<IPLhandle>,
    sa_hrtf_renderer: Cell<IPLhandle>,
    sa_input_format:  IPLAudioFormat,
    sa_output_format: IPLAudioFormat
}

unsafe extern "C" fn sdl_audio_callback (user_data: *mut raw::c_void, audio_stream: *mut u8, audio_length: i32) {

    // Get a reference to the audio system
    let audio_system = &*(user_data as *const AudioSystem);

    // Check the callback length in the audio system
    //
    // If it is 0, that means that the previous frame has finished playing, and we need to
    // wait until the new frame is available.
    if audio_system.callback_len.get () == 0 {
        return;
    }

    let audio_length = cmp::min (audio_length as usize, audio_system.callback_len.get ());

    // Copy the pre-processed audio buffer into SDL's internal buffer
    ptr::copy (audio_system.frame_buffer.as_ptr (), audio_stream, audio_length);
    audio_system.callback_len.set (
        audio_system.callback_len.get () - audio_length
    );
}

impl AudioSystem {

    pub fn new () -> AudioSystem {

        AudioSystem {

            audio_device:     Cell::new (0),
            callback_len:     Cell::new (0),

            clip_list:        HashMap::new (),
            emitter_list:     Vec::new (),
            listener_pos:     Point3::default (),

            frame_buffer:     [0; BUFFER_SIZE],

            sa_context:       Cell::new (ptr::null_mut ()),
            sa_hrtf_renderer: Cell::new (ptr::null_mut ()),

            sa_input_format: IPLAudioFormat {

                channelLayoutType:       IPLChannelLayoutType_IPL_CHANNELLAYOUTTYPE_SPEAKERS,
                channelLayout:           IPLChannelLayout_IPL_CHANNELLAYOUT_STEREO,
                channelOrder:            IPLChannelOrder_IPL_CHANNELORDER_INTERLEAVED,
                ambisonicsNormalization: IPLAmbisonicsNormalization_IPL_AMBISONICSNORMALIZATION_N3D,
                ambisonicsOrder:         0,
                ambisonicsOrdering:      0,
                numSpeakers:             0,
                speakerDirections:       ptr::null_mut ()
            },

            sa_output_format: IPLAudioFormat {

                channelLayoutType:       IPLChannelLayoutType_IPL_CHANNELLAYOUTTYPE_SPEAKERS,
                channelLayout:           IPLChannelLayout_IPL_CHANNELLAYOUT_STEREO,
                channelOrder:            IPLChannelOrder_IPL_CHANNELORDER_INTERLEAVED,
                ambisonicsNormalization: IPLAmbisonicsNormalization_IPL_AMBISONICSNORMALIZATION_N3D,
                ambisonicsOrder:         0,
                ambisonicsOrdering:      0,
                numSpeakers:             0,
                speakerDirections:       ptr::null_mut ()
            }
        }
    }

    pub fn init (&mut self) -> Result<(), ()> {

        self.init_sdl2 ()?;
        self.init_steam_audio ()?;

        Ok (())
    }

    pub fn update (&mut self) {

        // Check internal callback length
        // This is used to determine if SDL has finished playing the audio.
        if self.callback_len.get () > 0 {
            return;
        }

        self.callback_len.set (BUFFER_SIZE);

        // Clear the old buffer, and set a temp buffer
        self.frame_buffer   = [0; BUFFER_SIZE];
        let mut temp_buffer = [0; BUFFER_SIZE];

        for emitter in self.emitter_list.iter ().filter (|e| e.is_playing.get () == true) {

            // Get the file
            // If it fails, log and move onto the next iteration.
            let file = match self.clip_list.get_mut (&emitter.audio_clip) {

                Some (f) => f,
                None => {

                    warn! ("Failed to get clip \"{}\" for emitter\n\tClip does not exist", emitter.audio_clip);
                    continue;
                }
            };

            // Get the file offset for the current emitter
            // Continue on failure.
            match file.stream.seek (SeekFrom::Start (emitter.frame_offset.get () * BUFFER_SIZE as u64)) {

                Ok  (_) => emitter.frame_offset.set (emitter.frame_offset.get () + 1),
                Err (e) => {

                    warn! ("Failed to seek in audio file \"{}\"\n\t{}", emitter.audio_clip, e);
                    continue;
                }
            }

            // Read the next frame of audio data into the buffer
            match file.stream.read (&mut temp_buffer) {

                Ok (f) => {

                    // Check whether the emitter has reached the end of the file
                    // If so, determine whether to stop playing, or loop
                    if f < BUFFER_SIZE {

                        if emitter.is_looping.get () { emitter.frame_offset.set (0); }
                            else { emitter.stop () }
                    }
                }

                Err (e) => {

                    warn! ("Failed to get audio frame from file \"{}\"\n\t{}", emitter.audio_clip, e);
                    continue;
                }
            }

            if emitter.is_3d.get () {

                if self.apply_binaural_effect (&emitter, &mut temp_buffer).is_err () {
                    continue;
                }
            }

            // Mix the contents of the temp buffer into the frame buffer
            unsafe { SDL_MixAudioFormat (self.frame_buffer.as_mut_ptr (), temp_buffer.as_ptr (), AUDIO_F32 as u16, BUFFER_SIZE as u32, 128); }
        }
    }

    pub fn load_clip (&mut self, file_path: &str) -> Result<(), ()> {

        // Open the file
        info! ("Loading audio file \"{}\"", file_path);

        // Extract the file name
        let name = Path::new (file_path)
            .file_name ().unwrap ()
            .to_str    ().unwrap ()
            .to_string ();

        // Check if already loaded
        if self.clip_list.contains_key (&name) {

            info! ("Audio file \"{}\" already loaded", file_path);
            return Ok (());
        }

        let file = match File::open (file_path) {

            Ok  (f) => f,
            Err (e) => {

                error! ("Failed to open open audio file\n\t{}", e);
                return Err (());
            }
        };

        self.clip_list.insert (name, AudioClip { stream: file });
        Ok (())
    }

    pub fn create_emitter (&mut self, file: &str, position: Option<Point3>) -> Result<Rc<AudioEmitter>, ()> {

        // Check if audio file exists
        if !self.clip_list.contains_key (file) {

            warn! ("Could not create audio emitter\n\tClip \"{}\" does not exist", file);
            return Err (());
        }


        let mut effect: IPLhandle = ptr::null_mut ();
        unsafe {

            info! ("Creating HRTF effect for emitter");
            iplCreateBinauralEffect (self.sa_hrtf_renderer.get (), self.sa_input_format, self.sa_output_format, &mut effect);
            if effect.is_null () {

                error! ("Failed to create HRTF effect for emitter");
                return Err (());
            }
        }

        self.emitter_list.push (

            Rc::new (AudioEmitter {

                audio_clip: String::from (file),

                is_3d:      Cell::new (false),
                is_looping: Cell::new (false),
                is_playing: Cell::new (false),

                position:

                    RefCell::new (match position {

                        Some (p) => p,
                        None => Point3::default ()
                    }),

                hrtf_effect:  Cell::new (effect),
                frame_offset: Cell::new (0)
            })
        );

        Ok (self.emitter_list.last ().unwrap ().clone ())
    }

    fn init_sdl2 (&mut self) -> Result<(), ()> {

        // Start SDL audio
        info! ("Initializing SDL audio");
        if unsafe { SDL_InitSubSystem (SDL_INIT_AUDIO) } != 0 {

            error! ("Failed to initialize SDL audio");
            return Err (());
        }

        // Set the SDL audio spec
        // This specifies the format SDL should expect
        let audio_spec = SDL_AudioSpec {

            freq:     SAMPLE_RATE as i32,        // Frequency
            format:   AUDIO_F32 as u16,          // Audio format
            channels: 2,                         // Number of audio channels (2 for stereo)
            silence:  0,                         // Buffer silence (calculated by SDL)
            samples:  FRAME_SIZE as u16,         // Buffer size in samples
            padding:  0,                         // Padding between samples
            size:     0,                         // Buffer size in bytes (calculated by SDL)
            callback: Some (sdl_audio_callback), // Callback
            userdata: self as *const _ as *mut _ // User data (containers a pointer to the AudioSystem)
        };

        // Create the audio device
        info! ("Creating SDL audio device");
        self.audio_device = Cell::new (unsafe {
            SDL_OpenAudioDevice (ptr::null_mut (), 0, &audio_spec, ptr::null_mut (), 0)
        });

        if self.audio_device.get () < 1 {

            error! ("Failed to open audio device");
            return Err (());
        }

        // Start the SDL audio callback
        unsafe { SDL_PauseAudioDevice (self.audio_device.get (), 0) };
        Ok (())
    }

    fn init_steam_audio (&mut self) -> Result<(), ()> {

        // Create steam audio context
        info! ("Initializing Steam audio");
        info! ("Creating Steam audio context");
        unsafe { iplCreateContext (None, None, None, self.sa_context.get_mut ()) };

        if self.sa_context.get ().is_null () {

            error! ("Failed to create Steam audio context");
            return Err (());
        }

        // Set the hrtf rendering parameters
        let settings = IPLRenderingSettings {

            convolutionType: IPLConvolutionType_IPL_CONVOLUTIONTYPE_PHONON,
            frameSize:       FRAME_SIZE  as i32,
            samplingRate:    SAMPLE_RATE as i32
        };

        let params = IPLHrtfParams {

            type_:          IPLHrtfDatabaseType_IPL_HRTFDATABASETYPE_DEFAULT,
            hrtfData:       ptr::null_mut (),
            loadCallback:   None,
            lookupCallback: None,
            numHrirSamples: 0,
            unloadCallback: None
        };

        // Create the hrtf renderer and effect
        unsafe {

            info! ("Creating HRTF renderer");
            iplCreateBinauralRenderer (self.sa_context.get (), settings, params, self.sa_hrtf_renderer.get_mut ());
            if self.sa_hrtf_renderer.get ().is_null () {

                error! ("Failed to create HRTF renderer");
                return Err (());
            }
        }

        Ok (())
    }

    fn apply_binaural_effect (&self, emitter: &AudioEmitter, buffer: &mut [u8; BUFFER_SIZE]) -> Result<(), ()> {

        // Get the direction between the listener and the effect
        let direction = Point3::normalize (&Point3::direction (&self.listener_pos, &emitter.position.borrow ()));

        // Create the effect in / out buffers
        let ibuf = IPLAudioBuffer {

            format: self.sa_input_format,
            numSamples: FRAME_SIZE as i32,
            interleavedBuffer: buffer as *const _ as *mut _,
            deinterleavedBuffer: ptr::null_mut ()
        };

        let obuf = IPLAudioBuffer {

            format: self.sa_output_format,
            numSamples: FRAME_SIZE as i32,
            interleavedBuffer: buffer as *const _ as *mut _,
            deinterleavedBuffer: ptr::null_mut ()
        };

        // Set the hrtf position to the direction. and apply the effect
        let effect_pos = IPLVector3 {

            x: direction.x,
            y: direction.y,
            z: direction.z
        };

        unsafe { iplApplyBinauralEffect (emitter.hrtf_effect.get (), ibuf, effect_pos, IPLHrtfInterpolation_IPL_HRTFINTERPOLATION_NEAREST, obuf) };

        Ok (())
    }
}

impl Drop for AudioSystem {

    fn drop (&mut self) {

        info! ("Shutting down audio system");

        unsafe {

            iplDestroyBinauralRenderer (self.sa_hrtf_renderer.get_mut ());
            iplDestroyContext          (self.sa_context.get_mut       ());

            SDL_PauseAudioDevice (self.audio_device.get (), 1);
            SDL_CloseAudioDevice (self.audio_device.get ());
            SDL_QuitSubSystem    (SDL_INIT_AUDIO);
        }
    }
}
