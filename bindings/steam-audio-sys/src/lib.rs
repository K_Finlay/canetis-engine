/*===============================================================================================*/
// Copyright 2018 Fractured Cell, All Rights Reserved.
//
// THIS FILE IS A PART OF Canetis Engine.
// UNAUTHORISED COPYING OF THIS FILE, VIA ANY MEDIUM IS STRICTLY PROHIBITED.
/*===============================================================================================*/

#![allow (non_upper_case_globals)]
#![allow (non_camel_case_types)]
#![allow (non_snake_case)]

include! (concat! (env! ("OUT_DIR"), "/bindings.rs"));
