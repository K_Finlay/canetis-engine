/*===============================================================================================*/
// Copyright 2018 Fractured Cell, All Rights Reserved.
//
// THIS FILE IS A PART OF Canetis Engine.
// UNAUTHORISED COPYING OF THIS FILE, VIA ANY MEDIUM IS STRICTLY PROHIBITED.
/*===============================================================================================*/

extern crate bindgen;

use std::{
    env,
    path::PathBuf
};

fn main () {

    let dependency_path = PathBuf::from (format! ("{}/../dependencies", env::var ("CARGO_MANIFEST_DIR").unwrap ()));

    #[cfg (all (target_os = "linux", target_arch = "x86_64"))]
    println! ("cargo:rustc-link-search={}/lib/linux", dependency_path.display ());

    println! ("cargo:rustc-link-lib=phonon");

    let binding = bindgen::Builder::default ()
        .header    ("wrapper.h")
        .clang_arg (format! ("-I{}/include", dependency_path.display ()))
        .generate  ()
        .expect    ("Failed to generate binding");

    let out_path = PathBuf::from (env::var ("OUT_DIR").unwrap ());
    binding
        .write_to_file (out_path.join ("bindings.rs"))
        .expect ("Failed to write binding");
}
