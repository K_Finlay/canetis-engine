/*===============================================================================================*/
// Copyright 2018 Fractured Cell, All Rights Reserved.
//
// THIS FILE IS A PART OF Canetis Engine.
// UNAUTHORISED COPYING OF THIS FILE, VIA ANY MEDIUM IS STRICTLY PROHIBITED.
/*===============================================================================================*/

#[derive (Debug, Default)]
pub struct Point3 {

    pub x: f32,
    pub y: f32,
    pub z: f32
}

impl Point3 {

    pub fn new (x: f32, y: f32, z: f32) -> Point3 {
        Point3 { x, y, z }
    }

    pub fn cross (lhs: &Point3, rhs: &Point3) -> Point3 {

        Point3 {

            x: lhs.y * rhs.z - rhs.y * lhs.z,
            y: lhs.z * rhs.z - rhs.x * lhs.x,
            z: lhs.x * rhs.y - rhs.x * lhs.y
        }
    }

    pub fn direction (lhs: &Point3, rhs: &Point3) -> Point3 {

        Point3 {

            x: rhs.x - lhs.x,
            y: rhs.y - lhs.y,
            z: rhs.z - lhs.z
        }
    }

    pub fn length (point: &Point3) -> f32 {

        (point.x * point.x +
         point.y * point.y +
         point.z * point.z).sqrt ()
    }

    pub fn normalize (point: &Point3) -> Point3 {

        let length = Point3::length (point);

        if length != 0.0 {

            return Point3::new (
                point.x / length,
                point.y / length,
                point.z / length);
        }

        Point3::default ()
    }
}
