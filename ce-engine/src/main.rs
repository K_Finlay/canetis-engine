/*===============================================================================================*/
// Copyright 2018 Fractured Cell, All Rights Reserved.
//
// THIS FILE IS A PART OF Canetis Engine.
// UNAUTHORISED COPYING OF THIS FILE, VIA ANY MEDIUM IS STRICTLY PROHIBITED.
/*===============================================================================================*/

extern crate env_logger;

extern crate ce_audio as audio;
extern crate ce_math  as math;

use std::f32;

fn print_title () {

    println! ("   ___   _   _  _ ___ _____ ___ ___   ___ _  _  ___ ___ _  _ ___ ");
    println! ("  / __| /_\\ | \\| | __|_   _|_ _/ __| | __| \\| |/ __|_ _| \\| | __|");
    println! (" | (__ / _ \\| .` | _|  | |  | |\\__ \\ | _|| .` | (_ || || .` | _| ");
    println! ("  \\___/_/ \\_\\_|\\_|___| |_| |___|___/ |___|_|\\_|\\___|___|_|\\_|___|");
    println! ();
    println! (" ||-------------------------Audio Demo-------------------------||\n");
}

fn main () {

    print_title ();
    env_logger::init ();

    let mut system = audio::AudioSystem::new ();
    system.init ().unwrap ();

    system.load_clip ("Resources/lead.raw").unwrap ();
    system.load_clip ("Resources/bass.raw").unwrap ();
    system.load_clip ("Resources/pad.raw").unwrap  ();

    let track01 = system.create_emitter ("lead.raw", Some (math::Point3::new (-1.0, 1.0, -1.0))).unwrap ();
    let track02 = system.create_emitter ("bass.raw", Some (math::Point3::new ( 1.0, 0.0,  0.0))).unwrap ();
    let track03 = system.create_emitter ("pad.raw",  Some (math::Point3::new ( 0.0, 0.0,  1.0))).unwrap ();

    track01.is_3d.set (true);
    track02.is_3d.set (true);
    track03.is_3d.set (true);

    track01.is_looping.set (true);
    track02.is_looping.set (true);
    track03.is_looping.set (true);

    track01.play ();
    track02.play ();
    track03.play ();

    let mut track03_rot = 0.0;

    loop {

        system.update ();
        std::thread::sleep (std::time::Duration::from_millis (1));

        track03.position.replace (math::Point3::new (f32::sin (track03_rot), f32::sin (track03_rot / 2.0), f32::cos (track03_rot)));
        track03_rot += 0.001;
    }
}
